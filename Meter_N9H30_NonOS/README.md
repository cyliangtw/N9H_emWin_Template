# Meter Template on N9H30 NonOS Platform

This is the reference implementation on N9H30 NonOS platform.

### Changelog.pdf

N9H30 Meter change history.

### Nuvoton N9H30 Platform_Burn_Code.pdf

N9H30 Burn-code quick start guide.

### Meter_Reference_Implementation.pdf

N9H30 Meter user manual.

### Note

Only support Nuvoton demo board "NuDesign HMI-N9H30" + 7" 800x480 LCD "NuDesign TFT-LCD7".
